---
vim: et ts=2 sts=2 sw=2
title: Audit de logiciels libres
keywords: audit oss libre
date: 4 décembre 2018
author:
  - Marc Chantreux <marc.chantreux@renater.fr>
  - Bastien G. (Etalab)
  - Harmonie V. (DINSIC)
  - Marc C. (Unistra)
  - PY G. (Framasoft)
---

la SPA de validation

Planning
décembre : 
    [marc] on ouvre un dépôt sur git ;  
    [tous] on trouve un nom ;
    [bastien] éventuellement, première alpha, d'ici le 20/12
    [tous] à partir du 20 décembre, on ouvre à la liste
    [bastien] contacter la liste des 12 personnes, pour les inviter à contribuer fin décembre + janvier
    [marc] ouvrir dépôt sur framagit

A partir de Janvier : 
    [frama] mettre un peu de temps de dév pour lisser le site
    [tous] poursuivre 
    
Février :


Questions / Catégories

(réponses sous la forme comment? quoi lire? qui contacter?) 
(questions fermées + "je ne sais pas")


# spread the word
  * conférences
  * communications
  *  readme + doc ( references + tutos )
  * publicité/acces au sources
  * code of conduct

# code quality
* coding guidelines
* QA
* packaging


# communication
     * site web ?
     * multilingue ?
     * "comment contribuer ?"
     * 

# juridique
* Avez vous les droits sur le code ?
*savez-vous ce qu'est une licence libre ?
*Avez-vous choisi une licence libre pour votre projet ?





