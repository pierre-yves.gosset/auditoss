---
vim: et ts=2 sts=2 sw=2
title: Audit de logiciels libres
keywords: audit oss libre
author:
  - Marc Chantreux <marc.chantreux@renater.fr>
---

# Historique

Rencontres Framasoft / RENATER aux JRES 2017:

* Comment faire une audit "autocentré" : suis-je en capacité de me rendre compte si mon projet est ouvert à la contribution ou pas ?
* Comment faire pour rendre les projets identifiables et générer plus d'utilisation ?
* Projet : single-page ; 100 (?) questions ; réponses "oui/non/en cours" ; si "non", micro-aide pour orienter + "qui contacter ?" + "Quoi lire ?"

# Notes

* Comment éviter la crainte "houla, va falloir gérer ces contributeur⋅ices !" ?
* Comment éviter l'effet "labelisation" ?

#

Marc : développer les compagnons.
Frein : décideurs ("pourquoi payer pour les autres ?", "ouvrir, ça ne marche pas ?")
Recherche d'un "appui" d'une institution (reconnue comme légitime) qui permettrait de "donner confiance" (et de légitimer) les choix de licences.
  => Bastien : ça n'est pas le rôle de la DINSIC (parallèle : "la DINSIC ne peut pas préconiser un langage de programmation", respect du principe de subsidiarité).

Harmonie : (réutiliser VVL ?) - services juridiques connaissent mal la question des licences libres.

http://vvlibri.org/fr

Harmonie: monter une formation "licences" pour les services juridiques (des choses existent: à défricher)

blue hats:
    * créer une liste annonces@ pour les ouvertures de codes et releases 
      (idéalement
 il faut faire l'annonce d'intention ?)
    * identification des code budies
    * page datagouv: licences homologuées ... 

Ressources :
https://entrepreneur-interet-general.etalab.gouv.fr/docs/mini-guide-logiciel-libre.pdf
  * pourquoi ouvrir son code ?
  * choix licence

https://github.com/entrepreneur-interet-general/eig-link/blob/master/opensource.org
https://github.com/entrepreneur-interet-general/eig-link/blob/master/opensource-faq.org
https://bestpractices.coreinfrastructure.org/fr
https://www.writethedocs.org/

glossaire:
EIG: entrepreneurs d’intérêt général

cas concrets:
    * https://github.com/DGFiP/Test-Compta-Demat/tree/master/src/testeur
    * http://sympa.org
    
    
